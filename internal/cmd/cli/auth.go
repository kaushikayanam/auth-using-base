package cli

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"

	"github.com/go-chi/chi/v5"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/kaushikayanam/base/configs"
	"gitlab.com/kaushikayanam/base/interfaces/httpsrvr"
	"nononsensecode.com/auth/internal/pkg/interfaces/openapi"
)

var (
	cfgFile string
	cfg     *configs.Config
	authCmd = &cobra.Command{
		Use:   "auth",
		Short: "start the auth api server",
		Run:   initServer,
	}
)

func init() {
	cobra.OnInitialize(initConfig)
	authCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "configuration file in the yaml format. Default is ./config.yaml")
}

func initConfig() {
	fmt.Println("Finding and reading configuration...")
	if cfgFile != "" {
		viper.SetConfigFile(cfgFile)
	} else {
		viper.AddConfigPath("./")
		viper.SetConfigType("yaml")
		viper.SetConfigName("config")
	}

	viper.AutomaticEnv()

	if err := viper.ReadInConfig(); err != nil {
		fmt.Println(err)
		os.Exit(2)
	}

	cfg = new(configs.Config)
	if err := viper.Unmarshal(cfg); err != nil {
		fmt.Println(err)
		os.Exit(2)
	}
}

func initServer(cmd *cobra.Command, args []string) {
	cfgJson, err := json.MarshalIndent(cfg, "", "   ")
	if err != nil {
		panic(err)
	}
	fmt.Println(string(cfgJson))
	fmt.Println("Initializing http server...")
	cfg.Init()

	server := openapi.NewServer(cfg.Server.SqlVendor())
	httpsrvr.RunHTTPServer(cfg.Server.Address(), func(router chi.Router) http.Handler {
		return openapi.HandlerFromMux(server, router)
	}, []func(http.Handler) http.Handler{}, cfg.Server.Http.CorsAllowedOrigins, cfg.Server.Http.ApiPrefix)
}
