package user

type User struct {
	id        int32
	username  string
	firstName string
	lastName  string
	email     string
	password  string
}

//Suggestion: We need to put user creation rules here

func New(username, firstName, lastName, email, password string) User {
	return User{
		username:  username,
		firstName: firstName,
		lastName:  lastName,
		email:     email,
		password:  password,
	}
}

func UnmarshalFromPersistenceUnit(id int32, username, firstName, lastName, email, password string) User {
	return User{
		id:        id,
		username:  username,
		firstName: firstName,
		lastName:  lastName,
		email:     email,
		password:  password,
	}
}

func (u *User) ChangePassword(p string) {
	u.password = p
}

func (u User) ID() int32 {
	return u.id
}

func (u User) Username() string {
	return u.username
}

func (u User) FirstName() string {
	return u.firstName
}

func (u User) LastName() string {
	return u.lastName
}

func (u User) Email() string {
	return u.email
}

func (u User) Password() string {
	return u.password
}

func (u User) MatchPassword(p string) bool {
	return u.password == p
}
