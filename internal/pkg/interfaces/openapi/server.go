package openapi

import (
	"nononsensecode.com/auth/internal/pkg/application"
	"nononsensecode.com/auth/internal/pkg/domain/model/user"
	"nononsensecode.com/auth/internal/pkg/infrastructure/sqldbimpl"
)

type Server struct {
	userService application.UserService
}

func NewServer(dbType string) *Server {
	var userRepo user.Repository
	switch dbType {
	case "mysql":
		userRepo = sqldbimpl.MySQLUserRepository{}
	case "sqllite":
		userRepo = sqldbimpl.SQLiteUserRepository{}
	default:
		panic("there is no such repository for " + dbType)
	}

	return &Server{
		userService: *application.NewUserService(userRepo),
	}
}
